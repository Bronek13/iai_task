<?php

namespace App\Controller;

use App\Entity\Invoice;
use App\Entity\Product;
use App\Form\Invoice1Type;
use App\Form\InvoiceType;
use App\Repository\InvoiceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/panel/invoice")
 */
class InvoiceController extends AbstractController
{
    /**
     * @Route("/", name="invoice_index", methods={"GET"})
     */
    public function index(InvoiceRepository $invoiceRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        return $this->render('invoice/index.html.twig', [
            'invoices' => $invoiceRepository->findBy(['user' => $this->getUser()->getId()]),
        ]);
    }

    /**
     * @Route("/new", name="invoice_new", methods={"GET","POST"})
     */
    public function new(Request $request)
    {
        $invoice = new Invoice();

        $form = $this->createForm(InvoiceType::class, $invoice);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $invoice = $form->getData();

            if (!$invoice instanceof Invoice) {
                throw new BadRequestHttpException();
            }

            /** @var Product $product */
            foreach ($invoice->getProducts() as $product) {
                $nettoPrice = $product->getNettoPrice();
                $amount = $product->getAmount();
                $vatRate = $product->getVatRate() + 1;
                $product->setBruttoPrice($nettoPrice * $amount * $vatRate);
            }

            $invoice->setCreatedAt(new \DateTime());
            $invoice->setUser($this->getUser());

            $manager = $this->getDoctrine()->getManager();

            $manager->persist($invoice);
            $manager->flush();

            return $this->redirect($this->generateUrl('invoice_index'));

        }

        return $this->render("invoice/new.html.twig", [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{invoiceNumber}", name="invoice_show", methods={"GET"})
     */
    public function show(Invoice $invoice): Response
    {
        if ($invoice->getUser() !== $this->getUser()) {
            throw new NotFoundHttpException();
        }

        return $this->render('invoice/show.html.twig', [
            'invoice' => $invoice,
            'totalPrice' => $invoice->getTotalPrice()
        ]);
    }

    /**
     * @Route("/{invoiceNumber}/edit", name="invoice_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Invoice $invoice): Response
    {
        if ($invoice->getUser() !== $this->getUser()) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(Invoice1Type::class, $invoice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('invoice_index');
        }

        return $this->render('invoice/edit.html.twig', [
            'invoice' => $invoice,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{invoiceNumber}", name="invoice_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Invoice $invoice): Response
    {
        if ($invoice->getUser() !== $this->getUser()) {
            throw new NotFoundHttpException();
        }

        if ($this->isCsrfTokenValid('delete'.$invoice->getInvoiceNumber(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($invoice);
            $entityManager->flush();
        }

        return $this->redirectToRoute('invoice_index');
    }
}
