<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InvoiceRepository")
 */
class Invoice
{

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="invoice" ,cascade={"persist"}))
     */
    protected $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->date = new \DateTime();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="id")
     */
    private $invoiceNumber;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="invoices")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string")
     */
    private $city;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $paymentDate;

    /**
     * @ORM\Column(type="text")
     */
    private $seller;

    /**
     * @ORM\Column(type="text")
     */
    private $buyer;

    /**
     * Assert\Regex(
     *     pattern="^\d{24}$",
     *     match=false,
     *     message="Wrong bank account number"
     *     )
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $bankAccNr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @return mixed
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * @param mixed $invoiceNumber
     * @return Invoice
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = $invoiceNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Invoice
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return Invoice
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Invoice
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     * @return Invoice
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * @param mixed $paymentDate
     * @return Invoice
     */
    public function setPaymentDate($paymentDate)
    {
        $this->paymentDate = $paymentDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * @param mixed $seller
     * @return Invoice
     */
    public function setSeller($seller)
    {
        $this->seller = $seller;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBuyer()
    {
        return $this->buyer;
    }

    /**
     * @param mixed $buyer
     * @return Invoice
     */
    public function setBuyer($buyer)
    {
        $this->buyer = $buyer;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBankAccNr()
    {
        return $this->bankAccNr;
    }

    /**
     * @param mixed $bankAccNr
     * @return Invoice
     */
    public function setBankAccNr($bankAccNr)
    {
        $this->bankAccNr = $bankAccNr;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param mixed $notes
     * @return Invoice
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }

    public function addProduct(Product $product)
    {
        $product->setInvoice($this);
        $this->products->add($product);
    }

    public function removeProduct(Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * @return float
     */
    public function getTotalPrice(): float
    {
        $price = 0;
        foreach ($this->getProducts() as $product) {
            $price += $product->getbruttoPrice();
        }

        return $price;
    }

}