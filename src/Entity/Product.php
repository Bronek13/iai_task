<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
//    /**
//     * @ORM\ManyToOne(targetEntity="Invoice", inversedBy="user")
//     */
//    private $invoices;

    const VAT_RATES = ['23%' => 0.23, '8%' => 0.8 , '5%' => 0.5, '0%' => 0];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Invoice", inversedBy="products", cascade={"persist"}))
     * @ORM\JoinColumn(name="invoice_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $invoice;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $measurementUnit;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\Column(type="float")
     */
    private $nettoPrice;

    /**
     * @ORM\Column(type="float")
     */
    private $vatRate;

    /**
     * @ORM\Column(type="float")
     */
    private $bruttoPrice;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Product
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMeasurementUnit()
    {
        return $this->measurementUnit;
    }

    /**
     * @param mixed $measurementUnit
     * @return Product
     */
    public function setMeasurementUnit($measurementUnit)
    {
        $this->measurementUnit = $measurementUnit;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     * @return Product
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNettoPrice()
    {
        return $this->nettoPrice;
    }

    /**
     * @param mixed $nettoPrice
     * @return Product
     */
    public function setNettoPrice($nettoPrice)
    {
        $this->nettoPrice = $nettoPrice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }

    /**
     * @param mixed $vatRate
     * @return Product
     */
    public function setVatRate($vatRate)
    {
        $this->vatRate = $vatRate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBruttoPrice()
    {
        return $this->bruttoPrice;
    }

    /**
     * @param mixed $bruttoPrice
     * @return Product
     */
    public function setBruttoPrice($bruttoPrice)
    {
        $this->bruttoPrice = $bruttoPrice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param mixed $invoice
     * @return Product
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
        return $this;
    }
}